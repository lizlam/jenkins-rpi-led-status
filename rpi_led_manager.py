import RPi.GPIO as GPIO
import time

led=0

def setup():
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(led, GPIO.OUT)

def setBlue():
    global led
    led=23

def setRed():
    global led
    led=19

def light(seconds, num=1):
    setup() 
    for i in range(0, num):
        GPIO.output(led, True)
        time.sleep(seconds)
        GPIO.output(led, False)
        time.sleep(seconds)
    GPIO.cleanup()

def dot():
    light(.1)

def dash():
    light(1)

