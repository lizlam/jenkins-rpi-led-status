Jenkins RPi LED Status
======================

Raspberry Pi and Jenkins project to show stats of jobs using LED lights via GPIO

Requirements:
-------------

1. Jenkins Python API
2. GPIO module for Raspberry Pi

You can install the required libraries with the following commands:

`sudo apt-get install python-pip
sudo pip install jenkinsapi
sudo pip install RPIO`
