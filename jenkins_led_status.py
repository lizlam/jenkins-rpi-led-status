import rpi_led_manager
from jenkinsapi.jenkins import Jenkins

#jenkins_url = 'http://localhost:8080'
#jenkins_job = 'test-build'
jenkins_url = 'http://jenkins.nodejs.org:80'
jenkins_job = 'node-test'

def get_server_instance():
    global jenkins_url
    server = Jenkins(jenkins_url)
    return server

def get_latest_job():
    global jenkins_job
    server = get_server_instance()
    job = server[jenkins_job]
    latest = job.get_last_build()
    return latest

def get_latest_job_status():
    global current_status
    job = get_latest_job()
    status = job.get_status()
    current_status = status
    return status

def set_status_led():
    status = get_latest_job_status()
    if status == 'FAILURE':
        print status
        rpi_led_manager.setRed()
    if status == 'SUCCESS':
        print status
        rpi_led_manager.setBlue()
    rpi_led_manager.dash() 


print 'Jenkins Version: ' + get_server_instance().version
print get_latest_job()
status = get_latest_job_status()
set_status_led()
